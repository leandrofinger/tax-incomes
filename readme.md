# Tax Income

This is code challenge made with Laravel 6.0, PostgreSQL and Redis.

## Running
```
docker-compose up
```

After running the previous command, access `http://localhost:8000`.

## Details
- Data Sources: PostgreSQL (data), Redis (Cache).
- MVC using Laravel standard, with Blade template engine.
- The field `tax_rate` in County model is just to keep the current rate. To calculate to meet the requirements was used the field with same name in `Income` model
- I didn't develop the a way to input data, if necessary, use directly the database, or `database/seeds/SampleDataSeeder.php` file within the command `php artisan db:seed`
    - A good way to implement this input would be through a queue or an API. 
