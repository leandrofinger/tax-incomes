#!/bin/sh

if [ ! -f .env]; then
  cp .env.example .env
fi

cd /app

composer install
php artisan migrate
php artisan db:seed

php artisan serve --host 0.0.0.0
