<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToIncomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incomes', static function (Blueprint $table) {
            $table->index('tax_rate');
            $table->index('tax_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incomes', static function (Blueprint $table) {
            $table->dropIndex(['tax_rate']);
            $table->dropIndex(['tax_amount']);
        });
    }
}
