<?php

use App\County;
use App\State;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(County::class, static function (Faker $faker) {
    return [
        'name' => $faker->firstName(),
        'tax_rate' => $faker->randomFloat(2, 0, 30),
        'state_id' => static function () {
            return factory(State::class)->create()->id;
        },
    ];
});
