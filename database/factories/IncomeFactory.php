<?php

use App\County;
use App\Income;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Income::class, static function (Faker $faker) {
    return [
        'description' => $faker->text(),
        'amount' => $faker->randomFloat(2),
        'tax_rate' => $faker->randomFloat(2, 0, 30),
        'tax_amount' => $faker->randomFloat(2),
        'county_id' => static function () {
            return factory(County::class)->create()->id;
        },
    ];
});
