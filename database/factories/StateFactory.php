<?php

use App\State;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(State::class, static function (Faker $faker) {
    return [
        'name' => $faker->firstName(),
    ];
});
