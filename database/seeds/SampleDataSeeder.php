<?php

use App\County;
use App\Income;
use App\State;
use Illuminate\Database\Seeder;

class SampleDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(State::class, 5)->create()->each(static function ($state) {
            factory(County::class, 10)->create([
                'state_id' => $state->id,
            ])->each(static function ($county) {
                factory(Income::class, 20)->create([
                    'county_id' => $county->id,
                ]);
            });
        });
    }
}
