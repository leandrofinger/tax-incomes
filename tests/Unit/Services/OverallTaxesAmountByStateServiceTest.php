<?php

namespace Tests\Unit\Services;

use App\County;
use App\Income;
use App\Services\OverallTaxAmountByStateService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class OverallTaxesAmountByStateServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testRunService()
    {
        // Arrange
        $service = new OverallTaxAmountByStateService();

        $county1 = factory(County::class)->create();
        $county2 = factory(County::class)->create([
            'state_id' => $county1->state_id,
        ]);
        factory(Income::class)->create([
            'county_id' => $county1->id,
            'tax_amount' => 100,
        ]);
        factory(Income::class)->create([
            'county_id' => $county2->id,
            'tax_amount' => 200,
        ]);
        factory(Income::class)->create();

        Cache::shouldReceive('has')
            ->with(OverallTaxAmountByStateService::CACHE_KEY)
            ->andReturn(false);

        Cache::shouldReceive('add')
            ->once();

        // Act
        $overallTaxes = $service->run();

        // Assert
        $this->assertEquals(300, $overallTaxes[$county1->state_id]);
    }

    public function testCache()
    {
        // Arrange
        $service = new OverallTaxAmountByStateService();
        $county = factory(County::class)->create();

        Cache::shouldReceive('has')
            ->once()
            ->with(OverallTaxAmountByStateService::CACHE_KEY)
            ->andReturn(true);

        Cache::shouldReceive('get')
            ->once()
            ->with(OverallTaxAmountByStateService::CACHE_KEY)
            ->andReturn([1 => 400]);

        // Act
        $overallTaxes = $service->run();

        // Assert
        $this->assertEquals(400, $overallTaxes[$county->state_id]);
    }
}
