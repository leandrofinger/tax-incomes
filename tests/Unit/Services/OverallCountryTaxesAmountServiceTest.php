<?php

namespace Tests\Unit\Services;

use App\Income;
use App\Services\OverallTaxAmountByStateService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Services\OverallCountryTaxAmountService;
use Tests\TestCase;

class OverallCountryTaxesAmountServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testRunService()
    {
        // Arrange
        $service = new OverallCountryTaxAmountService(
            new OverallTaxAmountByStateService()
        );

        for ($i = 0; $i < 5; $i++) {
            factory(Income::class)->create([
                'tax_amount' => 100,
            ]);
        }

        // Act
        $overallTaxesAmount = $service->run();

        // Assert
        $this->assertEquals(500, $overallTaxesAmount);
    }
}
