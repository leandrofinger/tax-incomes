<?php

namespace Tests\Unit\Services;

use App\County;
use App\Income;
use App\Services\TaxAmountAverageByStateService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class TaxAmountAverageByStateServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testRunService()
    {
        // Arrange
        $service = new TaxAmountAverageByStateService();

        $county1 = factory(County::class)->create();
        $county2 = factory(County::class)->create([
            'state_id' => $county1->state_id,
        ]);
        factory(Income::class)->create([
            'county_id' => $county1->id,
            'tax_amount' => 200,
        ]);
        factory(Income::class)->create([
            'county_id' => $county2->id,
            'tax_amount' => 100,
        ]);
        factory(Income::class)->create();

        Cache::shouldReceive('has')
            ->once()
            ->with(TaxAmountAverageByStateService::CACHE_KEY)
            ->andReturn(false);

        Cache::shouldReceive('add')
            ->once();

        // Act
        $averageTaxes = $service->run();

        // Assert
        $this->assertEquals(150, $averageTaxes[$county1->state_id]);
    }

    public function testCache()
    {
        // Arrange
        $service = new TaxAmountAverageByStateService();

        $county = factory(County::class)->create();

        Cache::shouldReceive('has')
            ->once()
            ->with(TaxAmountAverageByStateService::CACHE_KEY)
            ->andReturn(true);

        Cache::shouldReceive('get')
            ->once()
            ->with(TaxAmountAverageByStateService::CACHE_KEY)
            ->andReturn([1 => 200]);

        // Act
        $averageTaxes = $service->run();

        // Assert
        $this->assertEquals(200, $averageTaxes[$county->state_id]);
    }
}
