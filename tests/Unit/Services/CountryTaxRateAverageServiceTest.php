<?php

namespace Tests\Unit\Services;

use App\County;
use App\Income;
use App\Services\CountryTaxRateAverageService;
use App\Services\TaxRateAverageByStateService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CountryTaxRateAverageServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testRunService()
    {
        // Arrange
        $service = new CountryTaxRateAverageService(
            new TaxRateAverageByStateService()
        );

        $county1 = factory(County::class)->create();
        factory(Income::class)->create([
            'county_id' => $county1->id,
            'tax_rate' => 3,
        ]);
        factory(Income::class)->create([
            'county_id' => $county1->id,
            'tax_rate' => 1,
        ]);

        $county2 = factory(County::class)->create();
        factory(Income::class)->create([
            'county_id' => $county2->id,
            'tax_rate' => 5,
        ]);
        factory(Income::class)->create([
            'county_id' => $county2->id,
            'tax_rate' => 3,
        ]);

        // Act
        $taxRateAverage = $service->run();

        // Assert
        $this->assertEquals(3, $taxRateAverage);
    }

    public function testDivisionByZeroHandler()
    {
        // Arrange
        $service = new CountryTaxRateAverageService(
            new TaxRateAverageByStateService()
        );

        // Act
        $taxRateAverage = $service->run();

        // Assert
        $this->assertEquals(0, $taxRateAverage);
    }
}
