<?php

namespace Tests\Unit\Services;

use App\County;
use App\Income;
use App\Services\TaxRateAverageByStateService;
use App\State;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class TaxRateAverageByStateServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testRunService()
    {
        // Arrange
        $service = new TaxRateAverageByStateService();
        $expectedCounty = factory(County::class)->create();
        factory(Income::class)->create([
            'county_id' => $expectedCounty->id,
            'tax_rate' => 1.5,
        ]);
        factory(Income::class)->create([
            'county_id' => $expectedCounty->id,
            'tax_rate' => 2.5,
        ]);
        factory(Income::class)->create();

        Cache::shouldReceive('has')
            ->once()
            ->with(TaxRateAverageByStateService::CACHE_KEY)
            ->andReturn(false);

        Cache::shouldReceive('add')
            ->once();

        // Act
        $averageTaxRates = $service->run();

        // Assert
        $this->assertEquals(2, $averageTaxRates[$expectedCounty->state_id]);
    }

    public function testStateWithoutIncomes()
    {
        // Arrange
        $service = new TaxRateAverageByStateService();
        $expectedCounty = factory(County::class)->create();

        Cache::shouldReceive('has')
            ->once()
            ->with(TaxRateAverageByStateService::CACHE_KEY)
            ->andReturn(false);

        Cache::shouldReceive('add')
            ->once();

        // Act
        $taxRateAverage = $service->run();

        // Assert
        $this->assertArrayHasKey($expectedCounty->state_id, $taxRateAverage);
        $this->assertEquals(0, $taxRateAverage[$expectedCounty->state_id]);
    }

    public function testCache()
    {
        // Arrange
        $service = new TaxRateAverageByStateService();
        $expectedCounty = factory(County::class)->create();

        Cache::shouldReceive('has')
            ->once()
            ->with(TaxRateAverageByStateService::CACHE_KEY)
            ->andReturn(true);

        Cache::shouldReceive('get')
            ->once()
            ->with(TaxRateAverageByStateService::CACHE_KEY)
            ->andReturn([1 => 3]);

        // Act
        $averageTaxRates = $service->run();

        // Assert
        $this->assertEquals(3, $averageTaxRates[$expectedCounty->state_id]);
    }
}
