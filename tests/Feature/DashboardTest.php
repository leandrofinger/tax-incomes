<?php

namespace Tests\Feature;

use Tests\TestCase;

class DashboardTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDashboardRendering()
    {
        // Arrange
        // ...

        // Act
        $response = $this->get('/');

        // Assert
        $response->assertViewIs('dashboard');
        $response->assertStatus(200);
    }
}
