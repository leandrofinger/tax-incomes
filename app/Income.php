<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Income extends Model
{
    public function county(): BelongsTo
    {
        return $this->belongsTo(County::class);
    }
}
