<?php

namespace App\Services;

class CountryTaxRateAverageService
{
    private $taxRateAverageByStateService;

    public function __construct(TaxRateAverageByStateService $taxRateAverageByStateService)
    {
        $this->taxRateAverageByStateService = $taxRateAverageByStateService;
    }

    public function run()
    {
        $taxRateAverageByState = $this->taxRateAverageByStateService->run();

        if (!count($taxRateAverageByState)) {
            return 0;
        }

        $averageSum = collect($taxRateAverageByState)->reduce(static function ($carry, $stateAverage) {
            return $carry + $stateAverage;
        }, 0);

        return round($averageSum / count($taxRateAverageByState), 2);
    }
}
