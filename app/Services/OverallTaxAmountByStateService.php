<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class OverallTaxAmountByStateService
{
    public const CACHE_KEY = 'OverallTaxAmountByState';

    public function run()
    {
        if (Cache::has(self::CACHE_KEY)) {
            return Cache::get(self::CACHE_KEY);
        }

        $result = DB::table("incomes")
            ->join('counties', 'counties.id', '=', 'incomes.county_id')
            ->selectRaw('counties.state_id, SUM(incomes.tax_amount) as sum')
            ->groupBy('counties.state_id')
            ->get();

        $overallTaxes = $result->reduce(static function ($carry, $record) {
            $carry[$record->state_id] = $record->sum;

            return $carry;
        }, []);

        Cache::add(self::CACHE_KEY, $overallTaxes, 3600 * 24);

        return $overallTaxes;
    }
}
