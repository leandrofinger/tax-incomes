<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class TaxRateAverageByStateService
{
    public const CACHE_KEY = 'TaxRateAverageByState';

    public function run()
    {
        if (Cache::has(self::CACHE_KEY)) {
            return Cache::get(self::CACHE_KEY);
        }

        $result = DB::table("counties")
            ->leftJoin('incomes', 'counties.id', '=', 'incomes.county_id')
            ->selectRaw('counties.state_id, COALESCE(AVG(incomes.tax_rate), 0) as average')
            ->groupBy('counties.state_id')
            ->get();

        $taxRateAverage = $result->reduce(static function ($carry, $record) {
            $carry[$record->state_id] = $record->average;

            return $carry;
        }, []);

        Cache::add(self::CACHE_KEY, $taxRateAverage, 3600 * 24);

        return $taxRateAverage;
    }
}
