<?php

namespace App\Services;

class OverallCountryTaxAmountService
{
    private $overallTaxesAmountByStateService;

    public function __construct(OverallTaxAmountByStateService $overallTaxesAmountByStateService)
    {
        $this->overallTaxesAmountByStateService = $overallTaxesAmountByStateService;
    }

    public function run()
    {
        $overallByStates = $this->overallTaxesAmountByStateService->run();
        return collect($overallByStates)->reduce(static function ($carry, $stateOverall) {
            return $carry + $stateOverall;
        }, 0);
    }
}
