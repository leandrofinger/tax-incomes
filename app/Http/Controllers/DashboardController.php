<?php

namespace App\Http\Controllers;

use App\Services\CountryTaxRateAverageService;
use App\Services\OverallCountryTaxAmountService;
use App\Services\OverallTaxAmountByStateService;
use App\Services\TaxAmountAverageByStateService;
use App\Services\TaxRateAverageByStateService;
use App\State;

class DashboardController extends Controller
{
    private $overallTaxAmountByStateService;
    private $taxAmountAverageByStateService;
    private $taxRateAverageByStateService;
    private $overallCountryTaxAmountService;
    private $countryTaxRateAverageService;

    public function __construct(
        OverallTaxAmountByStateService $overallTaxAmountByStateService,
        TaxAmountAverageByStateService $taxAmountAverageByStateService,
        TaxRateAverageByStateService $taxRateAverageByStateService,
        OverallCountryTaxAmountService $overallCountryTaxAmountService,
        CountryTaxRateAverageService $countryTaxRateAverageService
    )
    {
        $this->overallTaxAmountByStateService = $overallTaxAmountByStateService;
        $this->taxAmountAverageByStateService = $taxAmountAverageByStateService;
        $this->taxRateAverageByStateService = $taxRateAverageByStateService;
        $this->overallCountryTaxAmountService = $overallCountryTaxAmountService;
        $this->countryTaxRateAverageService = $countryTaxRateAverageService;
    }

    public function index()
    {
        $overallAmountByState = $this->overallTaxAmountByStateService->run();
        $taxAmountAverageByState = $this->taxAmountAverageByStateService->run();
        $taxRateAverageByState = $this->taxRateAverageByStateService->run();
        $countryTaxRateAverage = $this->countryTaxRateAverageService->run();
        $overallCountryTaxAmount = $this->overallCountryTaxAmountService->run();

        $states = State::all()->pluck('name', 'id');

        return view('dashboard', compact(
            'overallAmountByState',
            'taxAmountAverageByState',
            'taxRateAverageByState',
            'countryTaxRateAverage',
            'overallCountryTaxAmount',
            'states'
        ));
    }
}
