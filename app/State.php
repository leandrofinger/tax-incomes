<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class State extends Model
{
    public function counties(): HasMany
    {
        return $this->hasMany(County::class);
    }
}
