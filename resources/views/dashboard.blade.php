@extends('layouts.default')

@section('content')
<h1>Tax Income</h1>
<div class="row">
    <div class="col-sm-6 mb-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Overall Amount per State</h5>
                @if(count($overallAmountByState))
                    <table class="table table-borderless">
                        <tbody>
                        @foreach($overallAmountByState as $stateId => $stateAmount)
                            <tr>
                                <td>{{$states[$stateId]}}</td>
                                <td>$ {{number_format($stateAmount, 2)}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <span>No data available</span>
                @endif
            </div>
        </div>
    </div>

    <div class="col-sm-6 mb-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Tax Amount Average per State</h5>
                @if(count($taxAmountAverageByState))
                    <table class="table table-borderless">
                        <tbody>
                        @foreach($taxAmountAverageByState as $stateId => $stateAverage)
                            <tr>
                                <td>{{$states[$stateId]}}</td>
                                <td>$ {{number_format($stateAverage, 2)}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <span>No data available</span>
                @endif
            </div>
        </div>
    </div>

    <div class="col-sm-6 mb-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Tax Rate Average per State</h5>
                @if(count($taxRateAverageByState))
                    <table class="table table-borderless">
                        <tbody>
                        @foreach($taxRateAverageByState as $stateId => $stateTaxRate)
                            <tr>
                                <td>{{$states[$stateId]}}</td>
                                <td>$ {{number_format($stateTaxRate, 2)}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <span>No data available</span>
                @endif
            </div>
        </div>
    </div>

    <div class="col-sm-6 mb-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Country Tax Rate Average</h5>
                <span>$ {{number_format($countryTaxRateAverage, 2)}}</span>
            </div>
        </div>
    </div>

    <div class="col-sm-6 mb-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Overall Country Tax Amount</h5>
                <span>$ {{number_format($overallCountryTaxAmount, 2)}}</span>
            </div>
        </div>
    </div>
</div>
@endsection
